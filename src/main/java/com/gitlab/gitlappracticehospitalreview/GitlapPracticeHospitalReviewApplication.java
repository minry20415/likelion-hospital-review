package com.gitlab.gitlappracticehospitalreview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlapPracticeHospitalReviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlapPracticeHospitalReviewApplication.class, args);
    }

}
